FROM python:3.10.12

# Add some labels
LABEL author='Thales'
LABEL description='This image will be used to install and run the python app'

# Create a non-root User called "app"
RUN groupadd -r app && useradd -r -g app app

# Copy of the app folder
COPY app/ /app
COPY test/ /app/test

# This allows us to change the owner of files and connect as the user with non root rights that we will use for next commands
RUN chown app:app -R /app && chown app:app -R /home
USER app

WORKDIR /app

RUN pip3 install -r requirements.txt

EXPOSE 5000

# By running the container, we will run the python script
CMD ["python3", "app.py"]
